from route import app
from cfg import PORT, HOST


if __name__ == "__main__":
    app.run(host=HOST, port=PORT, debug=True)
