ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg", "gif", "bmp"}
WEIGHT_PATH = [
    "src/dr_app/prediction/weight/fold_1.h5",
    "src/dr_app/prediction/weight/fold_2.h5",
    "src/dr_app/prediction/weight/fold_3.h5",
    "src/dr_app/prediction/weight/fold_4.h5",
    "src/dr_app/prediction/weight/fold_5.h5",
]
STORAGE_PATH = "src/dr_app/storage"
HOST = "0.0.0.0"
PORT = "5000"
DB_URL = "postgresql+psycopg2://dr_diagnosis:5NHO20BgGdxZNaEF@10.0.1.45:5432/dr_diagnosis"
DB_SCHEMA = "dr_mgmt"

STAGE_INFO = {
    "0": {
        "title": "The system does not detect anything unusual from fundoscopy images."
    },
    "1": {
        "title": "Stage 1: Mild Nonproliferative Retinopathy",
        "lesion": [
            "Microaneurysms. They are small areas of balloon-like swelling in the retina's tiny blood vessels",
            "Hemorrhage",
            "Depression of secretions in the retina."
        ],
        "affect": [
            "See black spots",
            "Sense of color changes."
        ]
    },
    "2": {
        "title": "Stage 2: Moderate Nonproliferative Retinopathy",
        "lesion": ["Macular edema, blood vessels that nourish the retina are blocked"],
        "affect": ["Reaching this stage means a greater chance that the disease will affect vision"]
    },
    "3": {
        "title": "Stage 3: Severe Nonproliferative Retinopathy",
        "lesion": [
            "Many more blood vessels are blocked, depriving several areas of the retina of their blood supply",
            "Scar tissue forms"
        ],
        "affect": [
            "Eye floaters",
            "High chance of vision losss",
        ]
    },
    "4": {
        "title": "Stage 4: Proliferative Retinopathy",
        "lesion": [
            "Thin and weak neovascularizations grow in rentina",
            "Leak blood",
            "Scar tissue"
        ],
        "affect": [
            "As the scar tissue gets smaller, it can pull the retina away from the back of the eye. This is called retinal detachment. It can lead to permanent loss of both straight-ahead and side vision",
        ]
    }
}
