class Config(object):
    SECRET_KEY = '5f352388884c22463451387a0aec5d2f'
    SERVICE_SLUG = 'portal'
    OIDC_REQUIRE_VERIFIED_EMAIL = False
    OIDC_USER_INFO_ENABLED = True
    OIDC_SCOPES = ['openid', 'email', 'profile']
    OIDC_INTROSPECTION_AUTH_METHOD = 'client_secret_post'
    OIDC_CLIENT_SECRETS = 'src/dr_app/cfg/client_secrets_dev.json'
    OIDC_OPENID_REALM = 'dr'
    OIDC_ID_TOKEN_COOKIE_SECURE = False
    HANDLER = "RotatingFileHandler"
