from blueprint import db
from cfg import DB_SCHEMA
from uuid import uuid4
from sqlalchemy.dialects.postgresql import UUID


class DrStage(db.Model):
    __tablename__ = "dr_stage"
    __table_args__ = {"schema": DB_SCHEMA}

    _id = db.Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    _created = db.Column(db.DateTime(timezone=False))
    _deleted = db.Column(db.DateTime(timezone=False))
    code = db.Column(db.Integer)
    description = db.Column(db.String)

    def __init__(self, _id, _created, _deleted, code, description):
        self._id = _id
        self._created = _created
        self._deleted = _deleted
        self.code = code
        self.description = description

    def __repr__(self):
        return str({
            "_id": self._id,
            "_created": self._created,
            "_deleted": self._deleted,
            "code": self.code,
            "description": self.description
        })
