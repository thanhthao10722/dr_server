from blueprint import db
from sqlalchemy.dialects.postgresql import UUID
from cfg import DB_SCHEMA
from uuid import uuid4


class Diagnosis(db.Model):
    __tablename__ = "diagnosis"
    __table_args__ = {"schema": DB_SCHEMA}

    _id = db.Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    _created = db.Column(db.DateTime(timezone=False))
    _deleted = db.Column(db.DateTime(timezone=False))
    _creator = db.Column(UUID(as_uuid=True), default=uuid4)
    dr_stage_id = db.Column(UUID(as_uuid=True), default=uuid4)

    def __init__(self, _id, _created, _deleted, _creator, dr_stage_id):
        self._id = _id
        self._created = _created
        self._deleted = _deleted
        self._creator = _creator
        self.dr_stage_id = dr_stage_id

    def __repr__(self):
        return str({
            "_id": self._id,
            "_created": self._created,
            "_deleted": self._deleted,
            "_creator": self._creator,
            "dr_stage_id": self.dr_stage_id
        })
