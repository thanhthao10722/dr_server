from blueprint import db
from sqlalchemy.dialects.postgresql import UUID
from cfg import DB_SCHEMA
from uuid import uuid4


class User(db.Model):
    __tablename__ = "user"
    __table_args__ = {"schema": DB_SCHEMA}

    _id = db.Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    _created = db.Column(db.DateTime(timezone=False))
    _deleted = db.Column(db.DateTime(timezone=False))
    name__given = db.Column(db.String)
    name__family = db.Column(db.String)
    username = db.Column(db.String)
    keycloak_id = db.Column(UUID(as_uuid=True), default=uuid4)
    email = db.Column(db.String)

    def __init__(
        self, _id, _created,
        _deleted, name__given,
        name__family, keycloak_id,
        username, email
    ):
        self._id = _id
        self._created = _created
        self._deleted = _deleted
        self.name__given = name__given
        self.name__family = name__family
        self.keycloak_id = keycloak_id
        self.username = username
        self.email = email

    def __repr__(self):
        return str({
            "_id": self._id,
            "_created": self._created,
            "_deleted": self._deleted,
            "name__given": self.name__given,
            "name__family": self.name__family,
            "keycloak_id": self.keycloak_id,
            "username": self.username,
            "email": self.email
        })