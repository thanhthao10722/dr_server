from flask import request, render_template, redirect, url_for, g
from blueprint import app, db, oidc, _logger
from model import DrStage, Diagnosis, User
# from helper import predict_image
from uuid import uuid4
from datetime import datetime
from cfg import STAGE_INFO, STORAGE_PATH
import base64
import os


def check_user_session():
    if oidc.user_loggedin:
        user_info = oidc.user_getinfo(
            ["preferred_username", "email", "sub", "family_name", "given_name"]
        )
        user = User.query.filter(
            User.keycloak_id == user_info.get("sub")).first()
        g.user = user
        if not user:
            new_user = User(
                _id=uuid4(),
                _created=datetime.now(),
                keycloak_id=user_info.get("sub"),
                _deleted=None,
                name__family=user_info.get("family_name"),
                name__given=user_info.get("given_name"),
                username=user_info.get("preferred_username"),
                email=user_info.get("email")
            )
            db.session.add(new_user)
            db.session.commit()
            g.user = user
        return g.user
    else:
        return None


def read_file(file_path):
    file = open(file_path, "r")
    image = {{
        "content": base64.b64encode(file.read()).decode('utf-8'),
        "file_name": os.path.basename(file_path)
    }}
    file.close()
    return image


def save_file(file_content, file_extension, file_name):
    _tmp = f"{STORAGE_PATH}/{file_name}"
    f = open(_tmp, "wb")
    f.write(file_content)
    f.close()
    return _tmp


@app.route("/home-page")
def home_page():
    check_user_session()
    return render_template("index.html")


@app.route("/login")
@oidc.require_login
def login():
    _logger.info(
        "{} logged in successfully".format(oidc.user_getfield("email")))
    return redirect(request.args.get("next") or redirect(url_for("home_page")))


@app.route('/logout')
@oidc.require_login
def logout():
    email = oidc.user_getfield('email')
    oidc.logout()
    redirect_url = f"{request.url_root}home-page"
    keycloak_issuer = oidc.client_secrets.get('issuer')
    keycloak_logout_url = '{}/protocol/openid-connect/logout'.format(
        keycloak_issuer
    )
    _logger.info('{} logged out'.format(email))
    return redirect('{}?redirect_uri={}'.format(
        keycloak_logout_url,
        redirect_url
    ))


@app.route("/predict-dr", methods=["POST"])
def diagnose_dr():
    user = check_user_session()
    if user:
        file = request.files.get("file")
        data = file.read()
        extension = file.filename.rsplit('.', 1)[1].lower()
        # result = predict_image(data, extension)

        dr_stage = DrStage.query.filter(DrStage.code == result).first()
        prediction_id = uuid4()
        _tmp_filename = f"{prediction_id}.{extension}"
        _tmp = save_file(data, extension, _tmp_filename)
        prediction = Diagnosis(
            _id=prediction_id,
            _created=datetime.now(),
            dr_stage_id=dr_stage._id,
            _deleted=None,
            _creator=user._id,
            path=_tmp
        )
        db.session.add(prediction)
        db.session.commit()

        return redirect(url_for(
            "detail", predict_id=prediction._id, user_id=user._id))


@app.route("/guidance", methods=["GET"])
def guide():
    check_user_session()
    return render_template("guide.html")


@app.route("/upload-image", methods=["GET"])
def upload_image():
    user = check_user_session()
    if user:
        return render_template("upload.html")
    else:
        return redirect(url_for("login"), next=request.endpoint)


@app.route("/predict/<predict_id>/<user_id>/detail", methods=["GET"])
def detail(predict_id, user_id):
    user = check_user_session()
    if user and user._id == user_id:
        diagnosis = Diagnosis.query.get(predict_id)
        dr_stage = DrStage.query.get(diagnosis.dr_stage_id)
        stage_info = STAGE_INFO.get(str(dr_stage.code))
        image = read_file(diagnosis.path)
        return render_template(
            "detail.html", stage_info=stage_info, image=image
        )
    else:
        return redirect(url_for("login"), next=request.endpoint)
