import cv2
import numpy as np
from prediction import model


def predict_image(data, extension):
    nparr = np.fromstring(data, np.uint8)
    img_np = cv2.imdecode(nparr, flags=1)
    result = model.predict(img_np, extension)
    return result
