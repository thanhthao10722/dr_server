import warnings
from time import time
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    from .efficientnet_master.efficientnet import EfficientNetB5
    from cfg import WEIGHT_PATH, STORAGE_PATH
    import tensorflow as tf
    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    from keras.models import Model
    from keras.layers import Dense, GlobalAveragePooling2D, Input
    from datetime import datetime
    from keras.preprocessing.image import ImageDataGenerator
    import pandas as pd
    import numpy as np
    from .image_processing import preprocess_image
    import logging
    logging.basicConfig(level=logging.INFO)
    import os
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class DR_Model():
    height = 224
    width = 224
    channel = 3
    storage_path = STORAGE_PATH

    def __init__(self):
        self.model_list = self.load_weight(WEIGHT_PATH)

    def load_weight(self, path):
        model_list = []
        for weights_path in path:
            logging.info("---Load model----- %s", weights_path)
            model_list.append(self.create_model(weights_path))
        logging.info("------Models are created--------")
        self.graph = tf.get_default_graph()
        return model_list

    def create_model(self, weights_path):
        input_tensor = Input(shape=(self.height, self.width, self.channel))
        base_model = EfficientNetB5(weights=None,
                                    include_top=False,
                                    input_tensor=input_tensor)

        x = GlobalAveragePooling2D()(base_model.output)
        final_output = Dense(1, activation='softmax',
                             name='final_output')(x)
        model = Model(input_tensor, final_output)
        model.load_weights(weights_path)

        return model

    def classify(self, value):
        if value < 0.5:
            return 0
        elif value < 1.5:
            return 1
        elif value < 2.5:
            return 2
        elif value < 3.5:
            return 3
        return 4

    def apply_tta(self, model, generator, steps=5):
        start = time()
        preds_tta = []
        for i in range(steps):
            preds = model.predict_generator(generator, steps=1)
            preds_tta.append(preds)
        end = time()
        print(f"It took {end - start} seconds! to run apply_tta")
        return np.mean(preds_tta, axis=0)

    def test_ensemble_preds(self, generator, steps=5):
        start = time()
        preds_ensemble = []
        for model in self.model_list:
            with self.graph.as_default():
                preds = self.apply_tta(model, generator, steps)
                preds_ensemble.append(preds)
        end = time()
        print(f"It took {end - start} seconds! to run test_ensemble_preds ")
        return np.mean(preds_ensemble, axis=0)

    def predict(self, img, extension):
        start = time()
        _tmp_filename = f"{datetime.now()}.{extension}"
        _tmp = f"{self.storage_path}/{_tmp_filename}"

        preprocess_image(img, _tmp, self.height, self.width)
        data = {
            "0": [_tmp_filename]
        }
        datagen = ImageDataGenerator(rescale=1. / 255,
                                     rotation_range=360,
                                     horizontal_flip=True,
                                     vertical_flip=True)
        test_generator = datagen.flow_from_dataframe(
            dataframe=pd.DataFrame.from_dict(data, orient='index',
                                             columns=["id_code"]),
            directory=self.storage_path,
            x_col="id_code",
            batch_size=1,
            class_mode=None,
            shuffle=False,
            target_size=(self.height, self.width),
            seed=0)
        result = self.test_ensemble_preds(test_generator)
        end = time()
        print(f"It took {end - start} seconds! to run predict image functions")
        os.remove(_tmp)
        return self.classify(result[0])
