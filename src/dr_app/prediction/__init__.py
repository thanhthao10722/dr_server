from .dr_model import DR_Model


def __sif__():
    model = DR_Model()
    return model


model = __sif__()

__all__ = ("model")
