from flask_sqlalchemy import SQLAlchemy
from cfg import DB_URL, kc_config
from flask import Flask
from utils import configure_logger
from flask_oidc import OpenIDConnect


app = Flask(
    __name__,
    template_folder="templates",
    static_folder="templates/static"
)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
db.init_app(app)

oidc = OpenIDConnect()
app.config.from_object(kc_config)


_logger = configure_logger(app)
oidc.init_app(app)
