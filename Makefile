run:
	export FLASK_APP=__main__.py
	export FLASK_ENV=development
	export FLASK_DEBUG=ON
	PYTHONPATH=./src/dr_app python -m flask run -h $(TARGET_IP)

clean-py-binary:
	find ./src -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
