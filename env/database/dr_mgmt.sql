CREATE SCHEMA dr_mgmt;
-- ddl-end --

-- object: dr_mgmt."user" | type: TABLE --
-- DROP TABLE IF EXISTS dr_mgmt."user" CASCADE;
CREATE TABLE dr_mgmt."user" (
	_id uuid NOT NULL,
	_created timestamp,
	_deleted timestamp,
	name__given character varying,
	name__family character varying,
	name__middle character varying,
	keycloak_id uuid,
	CONSTRAINT user_pk PRIMARY KEY (_id)

);
-- ddl-end --

-- object: dr_mgmt.diagnosis | type: TABLE --
-- DROP TABLE IF EXISTS dr_mgmt.diagnosis CASCADE;
CREATE TABLE dr_mgmt.diagnosis (
	_id uuid NOT NULL,
	_created timestamp,
	_deleted timestamp,
	_creator uuid,
	dr_stage_id uuid,
	CONSTRAINT diagnosis_pk PRIMARY KEY (_id)

);
-- ddl-end --

-- object: dr_mgmt.dr_stage | type: TABLE --
-- DROP TABLE IF EXISTS dr_mgmt.dr_stage CASCADE;
CREATE TABLE dr_mgmt.dr_stage (
	_id uuid NOT NULL,
	code integer,
	description character varying,
	_created timestamp,
	_deleted timestamp,
	CONSTRAINT dr_stage_pk PRIMARY KEY (_id)

);
-- ddl-end --

-- object: user_fk | type: CONSTRAINT --
-- ALTER TABLE dr_mgmt.diagnosis DROP CONSTRAINT IF EXISTS user_fk CASCADE;
ALTER TABLE dr_mgmt.diagnosis ADD CONSTRAINT user_fk FOREIGN KEY (_creator)
REFERENCES dr_mgmt."user" (_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: stage_fk | type: CONSTRAINT --
-- ALTER TABLE dr_mgmt.diagnosis DROP CONSTRAINT IF EXISTS stage_fk CASCADE;
ALTER TABLE dr_mgmt.diagnosis ADD CONSTRAINT stage_fk FOREIGN KEY (dr_stage_id)
REFERENCES dr_mgmt.dr_stage (_id) MATCH SIMPLE
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

INSERT INTO "dr_mgmt"."dr_stage"(_id, code, description, _created)
VALUES ('23f4ab72-fa79-11ea-adc1-0242ac120002', 0 , 'Không mắc bệnh', '2020-09-19T13:10:07+00:00');
INSERT INTO "dr_mgmt"."dr_stage"(_id, code, description, _created)
VALUES ('bd4cc1b0-fa79-11ea-adc1-0242ac120002', 1 , 'Mild Nonproliferative Retinopathy', '2020-09-19T13:10:07+00:00');
INSERT INTO "dr_mgmt"."dr_stage"(_id, code, description, _created)
VALUES ('c73c583e-fa79-11ea-adc1-0242ac120002', 2 , 'Moderate Nonproliferative Retinopathy', '2020-09-19T13:10:07+00:00');
INSERT INTO "dr_mgmt"."dr_stage"(_id, code, description, _created)
VALUES ('d08d5816-fa79-11ea-adc1-0242ac120002', 3 , 'Severe Nonproliferative Retinopathy', '2020-09-19T13:10:07+00:00');
INSERT INTO "dr_mgmt"."dr_stage"(_id, code, description, _created)
VALUES ('d5dba2aa-fa79-11ea-adc1-0242ac120002', 4 , 'Proliferative Retinopathy', '2020-09-19T13:10:07+00:00')
